process.env.NODE_ENV = 'test';

const db = require('../db.js');
const { sequelize } = require('../db.js');
//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../index.js');
let Student = require('../student.js')

let should = chai.should();

var bulkStundeti = []
var bulkGrupe = []
var bulkVjezbe = []
var bulkZadaci = []

chai.use(chaiHttp);
//Our parent block
describe('Database', function() {
    this.timeout(5000)
    before((done) =>{

        sequelize.sync().then(function(){
            return db.zadatak.findAll().then(function(zadaci){
                zadaci.forEach(zadatak => {
                    zadatakObj ={
                        naziv:zadatak.naziv,
                        vjezbaId:zadatak.vjezbaId
                    }
                    bulkZadaci.push(zadatakObj)
                })
                return db.vjezba.findAll().then(function(vjezbe){
                    vjezbe.forEach(vjezba => {
                        vjezbaObj ={
                            naziv:vjezba.naziv,
                            brojZadataka:vjezba.brojZadataka
                        }
                        bulkVjezbe.push(vjezbaObj)
                    })
                    return db.grupa.findAll().then(function(grupe){
                        grupe.forEach(grupa => {
                            grupaObj ={
                                naziv:grupa.naziv
                            }
                            bulkGrupe.push(grupaObj)
                        })
                        return db.student.findAll().then(function(studenti){
                            studenti.forEach(student =>{
                                studentObj = {
                                    ime:student.ime,
                                    prezime:student.prezime,
                                    index:student.index,
                                    grupaId:student.grupaId
                                }
                                bulkStundeti.push(studentObj)
                            })
                            sequelize.sync({force:true}).then(function(){
                                done()
                            })
                        })
                    })
                })
            })
        })  

    })

    describe('/GET/vjezbe', () => {
      it('it should GET all the vježbe', (done) => {
        chai.request(server)
            .get('/vjezbe')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                done();
            })
        })
    })

    describe('/POST/vjezbe', () => {
        it('it should not POST vježbe ', (done) => {
        let vjezbe = {
            brojVjezbi:"-1",
            brojZadataka:"2,12"
        }
        chai.request(server)
          .post('/vjezbe')
          .set('Content-type', 'application/json')
          .send(vjezbe)
          .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('data').eql("Pogrešan parametar brojVjezbi,z1,brojZadataka")
                done();
            })
        })

        

      it('it should POST vježbe', (done) => {

            sequelize.sync({force:true}).then(function(){

                let vjezbe = {
                    brojVjezbi:"1",
                    brojZadataka:"2"
                }
                chai.request(server)
                    .post('/vjezbe')
                    .set('Content-type', 'application/json')
                    .send(vjezbe)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('brojVjezbi');
                        //done()
                    })
            }).catch(done())
        })
    })

    describe('/POST/student', () => {
        it('it should not POST student ', (done) => {
            db.student.create({ime:"Amna", prezime:"Jusić", index:"18391", grupaId:1}).then(function(s){
                let student = {
                    ime:"Amna",
                    prezime:"Jusić",
                    index:"18391",
                    grupa:"Grupa 1"
                }
                chai.request(server)
                  .post('/student')
                  .set('Content-type', 'application/json')
                  .send(student)
                  .end((err, res) => {
                        console.log(res.body)
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('status').eql("Student sa indexom 18391 već postoji!")
                        done();
                  })
            }).catch(done())
        })
        it('it should POST student ', (done) => {
            let student = {
                ime:"Amna",
                prezime:"Jusić",
                index:"18391",
                grupa:"Grupa 1"
            }
            chai.request(server)
            .post('/student')
            .set('Content-type', 'application/json')
            .send(student)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('status').eql("Kreiran student!")
                done();
            })
        })
    })

    describe('/PUT/:index student', () => {
        it('it should UPDATE a student given the index', (done) => {
        
            db.grupa.create({naziv:"Grupa 1"}).then(function(grupa){
                db.student.create({ime:'Amna', prezime:'Jusić', index:'18391', grupaId:grupa.dataValues.id}).then(function(student){
                    chai.request(server)
                    .put('/student/' + 18391)
                    .send({ime:'Amna', prezime:'Jusić', index:'18391', grupa:'Grupa 2'})
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('status').eql('Promjenjena grupa studentu 18391');
                        done();
                    });
                })
            })
        });

        it('it should not UPDATE a student given the index', (done) => {
            chai.request(server)
          .put('/student/' + 18391)
          .send({ime:'Amna', prezime:'Jusić', index:'18391', grupa:'Grupa 1'})
          .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('status').eql('Student sa indexom 18391 ne postoji');
                done();
          });
        })
    });

    describe('/POST/batch/student', () => {
        it('it should not POST students ', (done) => {
            db.student.create({ime:"Amna", prezime:"Jusić", index:"18391", grupaId:1}).then(function(s){
                let student = "Amna,Jusić,18391,Grupa 2"
                chai.request(server)
                  .post('/batch/student')
                  .set('Content-type', 'text/plain')
                  .send(student)
                  .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('status').eql("Dodano 0 studenata, a studenti 18391 već postoje!")
                        done();
                });
                
            }).catch(done())
        })
        it('it should POST students ', (done) => {
                let student = "Amna,Jusić,18391,Grupa 2\nArmena,Kojašević,18403,Grupa 1"
                chai.request(server)
                  .post('/batch/student')
                  .set('Content-type', 'text/plain')
                  .send(student)
                  .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('status').eql("Dodano 2 studenata!")
                    done();
                });
            
        });
    });

    afterEach((done) => { 
        sequelize.sync({force:true}).then(function(){
            done()
        })
    });

    after((done)=>{
        db.vjezba.bulkCreate(bulkVjezbe).then(function(){
            return db.zadatak.bulkCreate(bulkZadaci).then(function(){
                return db.grupa.bulkCreate(bulkGrupe).then(function(){
                    return db.student.bulkCreate(bulkStundeti).then(function(){
                        done()
                    })
                })
            })
        }) 
    })






});