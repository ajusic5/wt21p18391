var btn = document.getElementById("posalji")
btn.addEventListener('click', clickFun, true)
function clickFun(){

    var ime = document.getElementById("ime")
    var prezime = document.getElementById("prezime")
    var index = document.getElementById("index")
    var grupa = document.getElementById("grupa")


    if(ime.value == "" || prezime.value == "" || index.value == "" || grupa.value == ""){
        alert("Nijedno polje ne smije biti prazno!")
        return
    }

    var student = {"ime":ime.value, "prezime":prezime.value, "index":index.value, "grupa": grupa.value}
    StudentAjax.dodajStudenta(student, callbackFja)

}

let callbackFja = function (error, data) {
    var err;
    var d = {};
    if(error == null){
        err = null
        //d = {"brojVjezbi": data.brojVjezbi, "brojZadataka":data.brojZadataka}
    }
    else{
        err = error
        data = null
    }
}
