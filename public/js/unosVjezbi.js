var x = document.getElementById("myInput");

x.addEventListener("focusout", myFocusFunction, true);
function myFocusFunction() {
    var number = document.getElementById("myInput").value;
    var container = document.getElementById("container");
    VjezbeAjax.dodajInputPolja(container, number);
}

var y = document.getElementById("posalji");
y.addEventListener("click", clickFunction, true)
function clickFunction() {

    if(document.getElementById("myInput").value == ""){
        alert("Morate unijeti broj vježbi!");
        return;
    }
    
    let zadaci = "";
    var children = document.getElementById("container").children;

    for( var i = 0; i <children.length; i++ ){
        if(children[i].value >= 0 && children[i].value < 11)
        {
            if(i != children.length - 2)
            zadaci += children[i].value + "," 
            else
            zadaci += children[i].value 
        }
    }

    let vjezbeObjekat = {"brojVjezbi": document.getElementById("myInput").value, "brojZadataka": zadaci};
    VjezbeAjax.posaljiPodatke(vjezbeObjekat, callbackFja)

}

let callbackFja = function (error, data) {
    var err;
    var d = {};
    if(error == null){
        err = null
        d = {"brojVjezbi": data.brojVjezbi, "brojZadataka":data.brojZadataka}
    }
    else{
        err = error
        data = null
    }
}
