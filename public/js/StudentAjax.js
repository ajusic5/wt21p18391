let StudentAjax = (function(){
    
    const dodajStudenta = function(student, callbackFja){
        var ajax = new XMLHttpRequest();

        ajax.open("POST", "http://localhost:3000/student", true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify(student));

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200){
                callbackFja(null, ajax.status) 
            }
            
            if (ajax.readyState != 4 && ajax.status != 200){
                callbackFja(ajax.status,null) 
            }
        }
    }

    const postaviGrupu = function(index, grupa, callbackFja){
        var ajax = new XMLHttpRequest();
        ajax.open("PUT", "http://localhost:3000/student/"+index, true);
        
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200){
                callbackFja(null, ajax.responseText) 
            }
            
            if (ajax.readyState != 4 && ajax.status != 200){
                callbackFja(ajax.responseText,null) 
            }
        }
        ajax.setRequestHeader("Content-type", "application/json");
        ajax.send(JSON.stringify({"grupa":grupa}));
    }

    const dodajBatch = function(csvStudenti, callbackFja){ 

        var ajax = new XMLHttpRequest();
    //    console.log(csvStudenti)
    //    console.log(JSON.stringify(csvStudenti))

        ajax.open("POST", "http://localhost:3000/batch/student", true);
        ajax.setRequestHeader("Content-Type", "text/plain");
        ajax.send(csvStudenti);

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200){
                callbackFja(null, ajax.status) 
            }
            
            if (ajax.readyState != 4 && ajax.status != 200){
                callbackFja(ajax.status,null) 
            }
        }

    }

    return {
        dodajStudenta:dodajStudenta,
        postaviGrupu:postaviGrupu,
        dodajBatch:dodajBatch
    }
}());