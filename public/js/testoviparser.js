let TestoviParser = (function () {
    const dajTacnost = function (string) {

        var obj;
        try{
            obj = JSON.parse(string); 
        }
        catch(e){
            const rez ={"tacnost":"0%", "greske":["Testovi se ne mogu izvršiti"]};
            return rez;
        }
        
        for(var i = 0; i<obj.stats.tests; i++){
            if(Object.keys(obj.tests[i]).length != 7 ){
                const rez ={"tacnost":"0%", "greske":["Testovi se ne mogu izvršiti"]};
                return rez;
             
            }
        } 

        var tac = (obj.stats.passes/obj.stats.tests)*100; 

            if(!(Math.abs(tac - Math.trunc(tac)) < 0.00001)) tac = tac.toFixed(1).concat("%");
             
            else tac = tac.toString().concat("%");

            var g = [""];

            for(let i = 0; i<obj.stats.failures; i++){
                g.push( obj.failures[i].fullTitle);    
            }
            const greske = g.filter(str => str);
            const rez = {"tacnost":tac, "greske":greske};
            return rez;
        
    }
    const porediRezultate = function (rezultat1, rezultat2) { 

        var obj1, obj2;

        try{
            obj1 = JSON.parse(rezultat1);
            obj2 = JSON.parse(rezultat2);
        }
        catch(e){
            const rez ={"tacnost":"0%", "greske":["Testovi se ne mogu izvršiti"]};
            return rez;
        }

        for(var i = 0; i<obj1.stats.tests; i++){
            if(Object.keys(obj1.tests[i]).length != 7 ){
                const rez ={"tacnost":"0%", "greske":["Testovi se ne mogu izvršiti"]};
                return rez;
             
            }
        } 

        for(var i = 0; i<obj2.stats.tests; i++){
            if(Object.keys(obj2.tests[i]).length != 7 ){
                const rez ={"tacnost":"0%", "greske":["Testovi se ne mogu izvršiti"]};
                return rez;
             
            }
        } 

        if(obj1.stats.tests === obj2.stats.tests){

            var istiTestovi = true;
            var pomocniBool = false;
            var br = 0;
            var g1 = [""]; 

            for(let i = 0; i < obj1.stats.tests; i++){

                for(var j = 0; j < obj2.stats.tests; j++){

                    if(obj1.tests[i].fullTitle === obj2.tests[j].fullTitle){ 
                        pomocniBool = true;
                        
                    }
                }
                if(!pomocniBool){
                    istiTestovi = false;

                    for(var k = 0; k < obj1.stats.failures; k++){

                        if(obj1.failures[k].fullTitle === obj1.tests[i].fullTitle){
                            br++;
                            g1.push(obj1.failures[k].fullTitle);
                        }
                    }
                } 
                pomocniBool = false;
            }

            if(istiTestovi){

                var x = TestoviParser.dajTacnost(rezultat2);

                const greske = x.greske.filter(str => str);
                greske.sort();
                
                const rez = {"promjena": x.tacnost, "greske": greske};
                return rez;
            }

            else{
                var x = 100*(br + obj2.stats.failures)/(br + obj2.stats.tests);

                if(!(Math.abs(x - Math.trunc(x)) < 0.00001)) x = x.toFixed(1).concat("%"); 
                else x = x.toString().concat("%");
                
                var g2 = [""];

                for( var i = 0; i < obj2.stats.failures; i++){
                    g2.push(obj2.failures[i].fullTitle);
                }

                const gr1 = g1.filter(str => str);
                const gr2 = g2.filter(str => str);

                gr1.sort();
                gr2.sort();

                var greske = gr1.concat(gr2);
                const rez = {"promjena":x, "greske": greske};
                return rez;
            }
        
    }
    else{
        var pomocniBool2 = false;
        var br2 = 0;
        var grr1 = [""];
        
        for(let i = 0; i < obj1.stats.tests; i++){

            for(var j = 0; j < obj2.stats.tests; j++){

                if(obj1.tests[i].fullTitle === obj2.tests[j].fullTitle){ 
                    pomocniBool2 = true;
                    
                }
            }
            if(!pomocniBool2){
                
                for(var k = 0; k < obj1.stats.failures; k++){

                    if(obj1.failures[k].fullTitle === obj1.tests[i].fullTitle){
                        br2++;
                        grr1.push(obj1.failures[k].fullTitle);
                    }
                }
            } 
            pomocniBool2 = false;
        }

        var y = 100*(br2 + obj2.stats.failures)/(br2 + obj2.stats.tests);
            
        if(!(Math.abs(y - Math.trunc(y)) < 0.00001)) y = y.toFixed(1).concat("%"); 
        else y = y.toString().concat("%");
        
        var greske1 = grr1.filter(str => str);
        greske1.sort();

        var grr2 = [""];

        for( var i = 0; i < obj2.stats.failures; i++){
            grr2.push(obj2.failures[i].fullTitle);
        }
        var greske2 = grr2.filter(str => str);
        greske2.sort();
         
        var greskice = greske1.concat(greske2);
        var rez = {"promjena":y, "greske": greskice};
        return rez;
        }
    }
    return {
            dajTacnost: dajTacnost,
            porediRezultate: porediRezultate
        }
    }()
);