chai.should();
describe('VjezbeAjax testovi', function() {
    describe('Testovi dodajInputPolja', function(){
        it('Test dodajInputPolja - ispravan broj vježbi', function() {

    var div = document.createElement("div")
   
    VjezbeAjax.dodajInputPolja(div, 5)
    chai.assert.equal(div.children.length, 15) //za svako polje se dodaje labela i prečazak u novi red
        });

        it('Test dodajInputPolja - broj vježbi manji od 1', function() {

    var div = document.createElement("div")
   
    VjezbeAjax.dodajInputPolja(div, -35)
    chai.assert(div, null) 
        });

        it('Test dodajInputPolja - broj vježbi veći od 15', function() {

    var div = document.createElement("div")
   
    VjezbeAjax.dodajInputPolja(div, 35)
    chai.assert(div, null) 
        });
    });

    describe('Testovi iscrtajVjezbe', function(){
        it('Test iscrtajVjezbe - ispravan objekat', function() {

            var obj = {"brojVjezbi":5,"brojZadataka":[4,4,4,4,4]}
            var div = document.createElement("div")
        
            VjezbeAjax.iscrtajVjezbe(div, obj)
            chai.assert.equal(div.children.length, 5)
        });

        it('Test iscrtajVjezbe- provjera sadržaja buttona', function() {

            var obj = {"brojVjezbi":5,"brojZadataka":[4,4,4,4,4]}
            var div = document.createElement("div")
        
            VjezbeAjax.iscrtajVjezbe(div, obj)
            chai.assert.equal(div.children.length, 5)
            chai.assert.equal(div.children[1].textContent, "Vježba 2")
            chai.assert(!div.innerHTML.includes("Vježba 8"), "Vježbe idu od 1 do 5")
        });

        it('Test iscrtajVjezbe - više od 15 vježbi', function() {

            var obj = {"brojVjezbi":17,"brojZadataka":[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4]}
            var div = document.createElement("div")
        
            VjezbeAjax.iscrtajVjezbe(div, obj)
            chai.assert(div, null)
        });

        it('Test iscrtajVjezbe - manje od 1 vježbe', function() {

            var obj = {"brojVjezbi":0,"brojZadataka":[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4]}
            var div = document.createElement("div")
        
            VjezbeAjax.iscrtajVjezbe(div, obj)
            chai.assert(div, null)
        });

        it('Test iscrtajVjezbe - različit brojVjezbi i broj elemenata u nizu brojZadataka', function() {

            var obj = {"brojVjezbi":4,"brojZadataka":[4,4,4,4,4,4,4,4,4,4,4]}
            var div = document.createElement("div")
        
            VjezbeAjax.iscrtajVjezbe(div, obj)
            chai.assert(div, null)
        });

        it('Test iscrtajVjezbe - isti brojVjezbi i broj elemenata u nizu brojZadataka, ali jedan negativan brojZadataka', function() {

            var obj = {"brojVjezbi":4,"brojZadataka":[4,4,-31,4]}
            var div = document.createElement("div")
        
            VjezbeAjax.iscrtajVjezbe(div, obj)
            chai.assert(div, null)
        });

        it('Test iscrtajVjezbe - isti brojVjezbi i broj elemenata u nizu brojZadataka, ali jedan brojZadataka veći od 10', function() {

            var obj = {"brojVjezbi":4,"brojZadataka":[4,4,31,4]}
            var div = document.createElement("div")
        
            VjezbeAjax.iscrtajVjezbe(div, obj)
            chai.assert(div, null)
        });

        it('Test iscrtajVjezbe - više ispravnih objekata s različitim brojem vježbi', function() {

            var obj = {"brojVjezbi":5,"brojZadataka":[4,4,4,4,4]}
            var div = document.createElement("div")
        
            VjezbeAjax.iscrtajVjezbe(div, obj)
            chai.assert.equal(div.children.length, 5)

            obj = {"brojVjezbi":3, "brojZadataka":[1,2,3]}
            VjezbeAjax.iscrtajVjezbe(div, obj)
            chai.assert.equal(div.children.length, 3)

            obj = {"brojVjezbi":4, "brojZadataka":[1,2,3,4]}
            VjezbeAjax.iscrtajVjezbe(div, obj)
            chai.assert.equal(div.children.length, 4)
        });
    });

    describe('Testovi iscrtajZadatke', function(){
        it('Test iscrtajZadatke - ispravni podaci i pozivanje iscrtajZadatke s različitim brojem zadataka nad istom vježbom', function() {
            let div = document.createElement("div")
          
            VjezbeAjax.iscrtajZadatke(div, 4)
            chai.assert.equal(div.lastChild.textContent, "Zadatak 1Zadatak 2Zadatak 3Zadatak 4")
            
            VjezbeAjax.iscrtajZadatke(div, 5)
            chai.assert.equal(div.lastChild.textContent, "Zadatak 1Zadatak 2Zadatak 3Zadatak 4Zadatak 5")
            
            VjezbeAjax.iscrtajZadatke(div, 4)
            chai.assert.equal(div.lastChild.textContent, "Zadatak 1Zadatak 2Zadatak 3Zadatak 4")
            
        });

        it('Test iscrtajZadatke - neispravni podaci, broj manji od 0', function() {

            
            var div = document.createElement("div")
            
        
            VjezbeAjax.iscrtajZadatke(div, -2)
            chai.assert.equal(div.children.length, 0)
            
        });

        it('Test iscrtajZadatke - neispravni podaci, broj veći od 10', function() {

           
            var div = document.createElement("div")
            
        
            VjezbeAjax.iscrtajZadatke(div, 18)
            chai.assert.equal(div.children.length, 0)
            
        }); 
    });

    describe('Test dohvatiPodatke', function(){

        beforeEach(function() {
            this.xhr = sinon.useFakeXMLHttpRequest();
        
            this.requests = [];
            this.xhr.onCreate = function(xhr) {
            this.requests.push(xhr);
            }.bind(this);
        });
        
        afterEach(function() {
            this.xhr.restore();
        });

        it('Test uzimanja podataka', function(){
            var data = {"brojVjezbi":6, "brojZadataka":[4,4,4,4,4,5]}
            var dataJSON = JSON.stringify(data)

            VjezbeAjax.dohvatiPodatke(function(error, result){
                chai.assert(JSON.stringify(result) === dataJSON, "Podaci se ne poklapaju")
                chai.assert(error === null, "Nije dobijen error")   
            });

            this.requests[0].respond(200, {"Content-Type": "text/json"}, dataJSON)
        });

    });

    describe('Test posaljiPodatke', function(){

        beforeEach(function() {
            this.xhr = sinon.useFakeXMLHttpRequest();
        
            this.requests = [];
            this.xhr.onCreate = function(xhr) {
            this.requests.push(xhr);
            }.bind(this);
        });
        
        afterEach(function() {
            this.xhr.restore();
        });

        it('Test slanja podataka - ispravni podaci', function(){

            var data = {"brojVjezbi":2, "brojZadataka":[2,2]}
            var dataJSON = JSON.stringify(data)

            VjezbeAjax.posaljiPodatke({"brojVjezbi":2, "brojZadataka":[2,2]}, function(error, data){
                
            });
            this.requests[0].requestBody.should.equal(dataJSON)
            this.requests[0].respond(200, {"Content-Type": "text/json"}, dataJSON)
        });

        it('Test slanja podataka - pogrešan broj vježbi, jednog od broja zadataka na vježbama i broja elemenata', function(){

            var data = {"brojVjezbi":-8, "brojZadataka":[2,2,-1]}
            var dataJSON = JSON.stringify(data)

            VjezbeAjax.posaljiPodatke({"brojVjezbi":-8, "brojZadataka":[2,2,-1]}, function(error, data){
                
            });
            this.requests[0].requestBody.should.equal(dataJSON)
            this.requests[0].respond("error", {"Content-Type": "text/json"}, "Pogrešan parametar brojVjezbi,z2,brojZadataka")
        });

    });

});