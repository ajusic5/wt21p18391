let VjezbeAjax = (function(){
    
    var names = [];
    const dodajInputPolja = function(DOMelementDIVauFormi, brojVjezbi){

    if(brojVjezbi > 15 || brojVjezbi < 1)
    return;
        
        while (DOMelementDIVauFormi.hasChildNodes()) {
            DOMelementDIVauFormi.removeChild(DOMelementDIVauFormi.lastChild);
        } 
        for (var i=0; i < brojVjezbi; i++){
            
            var label = document.createElement("label")
            label.innerHTML = "z" + (i+1);
            DOMelementDIVauFormi.appendChild(label);
            
            var input = document.createElement("input");
            input.type = "number";
            input.name = "z" + i;
            input.value = 4
            DOMelementDIVauFormi.appendChild(input);
            
            DOMelementDIVauFormi.appendChild(document.createElement("br"));
        } 
    }

    const posaljiPodatke = function (vjezbeObjekat,callbackFja) {
        
        var ajax = new XMLHttpRequest();

        ajax.open("POST", "http://localhost:3000/vjezbe", true);

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200){
                
                callbackFja(null, {brojVjezbi:vjezbeObjekat.brojVjezbi, brojZadataka:vjezbeObjekat.brojZadataka}) 
            }
            
            if (ajax.readyState != 4 && ajax.status != 200){
                callbackFja(ajax.status,null) 
            }

        }

        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify(vjezbeObjekat));

    }

    const dohvatiPodatke = function(callbackFja){
        var ajax = new XMLHttpRequest();

        ajax.open("GET", "http://localhost:3000/vjezbe", true);
        ajax.send();

        ajax.onreadystatechange = function() {
            
            if (ajax.readyState == 4 && ajax.status == 200){
                var obj = JSON.parse(ajax.responseText)
                callbackFja(null, obj)  

            }
            if (ajax.readyState != 4 && ajax.status != 200){
                callbackFja(ajax.responseText,null) 
            }
        }
        
    }

    const iscrtajVjezbe = function(divDOMelement,objekat){
        if(objekat.brojVjezbi > 15 || objekat.brojVjezbi < 1)
            return;
        if(objekat.brojVjezbi != objekat.brojZadataka.length)
            return;
        
        for(var i = 0; i < objekat.brojZadataka.length; i++){
            if(objekat.brojZadataka[i] < 0 || objekat.brojZadataka[i] > 10)
            return;
        }
        while(divDOMelement.hasChildNodes()){
            divDOMelement.removeChild(divDOMelement.lastChild)
        }

        for(let i = 0; i < objekat.brojVjezbi; i++){

            let newDiv = document.createElement("div")
            newDiv.className="vjezba "+i

            let exBtn = document.createElement("button")
            exBtn.type="button"
            exBtn.className="btn"
            exBtn.id="Vjezba" + (i+1)
            exBtn.textContent="Vježba "+(i+1)

            let num = objekat.brojZadataka[i]
            newDiv.appendChild(exBtn)
            exBtn.onclick = function() {iscrtajZadatke(newDiv, num)}
            divDOMelement.appendChild(newDiv)
                
        }
        
    }

    const iscrtajZadatke = function(vjezbaDOMelement, brojZadataka) {

        if(brojZadataka < 0 || brojZadataka > 10)
        return;

        var pozvana = false
        for(var i = 0; i < names.length; i++){
            
            if(vjezbaDOMelement.className == names[i]){
                
                if(vjezbaDOMelement.lastChild.children.length < brojZadataka){
                    var num = vjezbaDOMelement.lastChild.children.length + 1;
                    while(vjezbaDOMelement.lastChild.children.length != brojZadataka){
                        
                        let newTaskBtn = document.createElement("button")
                        newTaskBtn.type="button"
                        newTaskBtn.id="z" + num
                        newTaskBtn.name="Zadatak" + num
                        newTaskBtn.textContent="Zadatak " + num
                        newTaskBtn.className="btn2"
                        vjezbaDOMelement.lastChild.appendChild(newTaskBtn)
                        num += 1
                    }
                }
                else{
                    while(vjezbaDOMelement.lastChild.children.length != brojZadataka){
                        vjezbaDOMelement.lastChild.removeChild(vjezbaDOMelement.childNodes[0].lastChild) 
                    }
                } 

                if(vjezbaDOMelement.lastChild.style.display == "none"){
  
                    vjezbaDOMelement.lastChild.style.display = "grid"
                    pozvana = true
                    }
                    else{ 
                    vjezbaDOMelement.lastChild.style.display = "none"
                    pozvana = true
                } 
            }
            else if(document.getElementsByClassName(names[i]).item(0).childNodes[1].style.display="grid"){
                document.getElementsByClassName(names[i]).item(0).childNodes[1].style.display="none"
            }
            
        }
        if(!pozvana){
            let newDiv = document.createElement("div")
            newDiv.className="container-inner"
            newDiv.id="zadaci"
            
            for(var i = 0; i < brojZadataka; i++){
           
                let newTaskBtn = document.createElement("button")
                newTaskBtn.type="button"
                newTaskBtn.id="z" + i
                newTaskBtn.name="Zadatak" + (i+1)
                newTaskBtn.textContent="Zadatak " + (i+1)
                newTaskBtn.className="btn2"
                newDiv.appendChild(newTaskBtn)
            
            }
            names.push(vjezbaDOMelement.className)
            vjezbaDOMelement.append(newDiv)
        } 
    }
    return {
        dodajInputPolja: dodajInputPolja,
        posaljiPodatke: posaljiPodatke,
        dohvatiPodatke: dohvatiPodatke,
        iscrtajVjezbe: iscrtajVjezbe,
        iscrtajZadatke: iscrtajZadatke}
    }
());