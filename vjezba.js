const Sequelize = require("sequelize");

module.exports = function(sequelize,DataTypes){
    const Vjezba = sequelize.define("vjezba",{
        naziv:Sequelize.STRING,
        brojZadataka:Sequelize.INTEGER
    })
    return Vjezba;
};