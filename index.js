const express = require('express');
const bodyParser = require('body-parser');
const path  = require("path");
const db = require('./db.js');
const { sequelize } = require('./db.js');

const app = express();
sequelize.sync()
app.use(bodyParser.json());
app.use(bodyParser.text())
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));
app.use(express.static(path.join(__dirname, "public")));
app.use('/', express.static(path.join(__dirname, 'public/html')));
app.use('/', express.static(path.join(__dirname, 'public/css')));
app.use('/', express.static(path.join(__dirname, 'public/js')));
app.use('/', express.static(path.join(__dirname, 'public/images'))); 

app.get("/vjezbe/", function(req, res){

    db.sequelize.sync()
    .then(function() {
      return db.vjezba.findAll();
    })
    .then(function(vjezbe) {

        let tasks = [];
       for(var i=0; i<vjezbe.length; i++){

            var obj = JSON.parse(JSON.stringify(vjezbe[i]))
            tasks.push(obj.brojZadataka);
        } 
        let objs ={brojVjezbi:vjezbe.length, brojZadataka:tasks}
        res.send(objs)
     })
});

app.post("/vjezbe/", function(req, res){
    let wrongParams = "Pogrešan parametar ";
    let wrongNum = false; 
    
    var numTasks = req.body.brojZadataka.split(',');

    if(req.body.brojVjezbi < 1 || req.body.brojVjezbi > 15){
        wrongParams += "brojVjezbi";
        wrongNum = true;
    }

    for(var i = 0; i < numTasks.length; i++){

        if(parseInt(numTasks[i]) < 0 || parseInt(numTasks[i]) > 10){
        
            if(wrongNum){
                wrongParams += ",z";
                wrongParams += i.toString();
            }
            else{
                wrongParams += "z";
                wrongParams += i.toString();
                wrongNum = true;
            }
        }
    }

    if(req.body.brojVjezbi != numTasks.length){
        if(wrongNum)
        wrongParams += ",brojZadataka";
        else{
            wrongParams += "brojZadataka";
            wrongNum = true;
        }
    }

    if(wrongNum){
    res.send({"status": "error", "data": wrongParams});
    res.end();
    }

    else{
        
       sequelize.query('SET FOREIGN_KEY_CHECKS=0').then(function(){
            return db.zadatak.sync({force:true}).then(function(){
                return db.vjezba.sync({force:true}).then(function(){
                   return sequelize.query('SET FOREIGN_KEY_CHECKS = 1')
            }).then(function(){

                var vjezbeBulk = []
                for(var i = 0; i < req.body.brojVjezbi; i++){
                    var num = parseInt(numTasks[i])
                    vjezbeBulk.push({naziv:'Vježba' + (i+1), brojZadataka:num})
                }
                    db.vjezba.bulkCreate(vjezbeBulk).then(function(vjezba){
                        var zadaciBulk = []

                        for(var j=0; j < vjezba.length; j++){
                            
                            var zadaciBulk = []

                            for(var k=0; k<vjezba[j].dataValues.brojZadataka; k++){
                                zadaciBulk.push({naziv:'Zadatak'+(k+1), vjezbaId:vjezba[j].dataValues.id})
                            }

                            db.zadatak.bulkCreate(zadaciBulk)
                        }
                    })
                })
            })
        })
        let list = "";
        for(var k = 0; k < req.body.brojVjezbi; k++){
            list += numTasks[k];
            if(k != req.body.brojVjezbi - 1) list+=",";
        }
        const listObj = {"brojVjezbi": req.body.brojVjezbi, "brojZadataka": list};
        res.send(listObj);
    }
});

app.post("/student", function(req, res){

    db.student.findOne({where:{index:req.body.index}}).then(function(nadjen){
        if(nadjen!=null){
            res.send({"Status":"Student s indexom " + req.body.index + " već postoji!"})
            res.end()
            return new Promise(function(resolve, reject){
                resolve(nadjen)
            })
        }
        else{
            db.grupa.findOrCreate({where:{naziv:req.body.grupa}}).then(function(nadjena){
                db.student.create({ime:req.body.ime, prezime:req.body.prezime, index:req.body.index, grupaId:nadjena[0].dataValues.id}).then(function(student){
                    res.send({"status":"Kreiran student!"});
                    return new Promise(function(resolve, reject){
                        resolve(student)
                    })
                })
                return new Promise(function(resolve, reject){
                    resolve(nadjena)
                })
            })
        }
    })
});

app.put("/student/:index", function(req, res){
    let ind = req.params.index
    db.student.findOne({where:{index:ind}}).then(function(student){
        if(student==null){
            res.send({"status":"Student sa indexom "+ ind + " ne postoji"})
        }
        else{
            db.grupa.findOrCreate({where:{naziv:req.body.grupa}}).then(function(nadjena){
                student.update({grupaId:nadjena[0].dataValues.id}).then(function(student){
                    res.send({"status":"Promjenjena grupa studentu " + student.dataValues.index})
                    return new Promise(function(resolve, reject){
                        resolve(student)
                    })
                })
                return new Promise(function(resolve, reject){
                    resolve(nadjena)
                })
            })
        }
    })
})

app.post("/batch/student", function(req, res){
   
    let vecUneseni = []
    let imena = []
    let prezimena = []
    let grupe = []
    let indexi = []
    let indexs =""
    
    let s = req.body.split(/\r|\n/)
    
    for(var i = 0; i < s.length; i++){
        if(s[i]!=''){
            let students = s[i].split(',')
            var ime = students[0] 
            var prezime = students[1]
            var index = students[2]
            var grupa = students[3] 

            if( students.length == 4 && ime != '' && prezime!='' && index != '' && grupa != '' && !indexs.includes(students[2])){

                if(!indexs.includes(index)){
                    imena.push(ime)
                    prezimena.push(prezime)
                    indexi.push(index)
                    grupe.push(grupa)
                    indexs += index
                    indexs += " "
                }
            }
        }
    } 
  
   return new Promise(function(resolve, reject){
    
    var studentiPromises = []
    for(var i = 0; i < imena.length; i++){
        var studentPromise = db.student.findOne({where:{index:indexi[i]}}).then(function(s){
            return new Promise(function(resolve, reject){
                resolve(s)
            })
        })
        studentiPromises.push(studentPromise)
    }

    Promise.all(studentiPromises).then(function(students){
        grupePromises = []
        var grupes = ""
        for(var j=0; j<students.length; j++){
            if(students[i] == null && !grupes.includes(grupe[j])){
                grupePromises.push(db.grupa.findOrCreate({where:{naziv:grupe[j]}}).then(function(g){
                    return new Promise(function(resolve, reject){
                        resolve(g)
                    })
                }))
                grupes += grupe[j]
                grupes += " "
            }
            
        }
        Promise.all(grupePromises).then(function(grupee){
            zaDodatiStudenti = []
           
            for(var k=0; k<students.length; k++){
                
                if(students[k] == null){
                    var id 
                    for(var l=0; l<grupee.length; l++){
                        if(grupee[l][0].dataValues.naziv == grupe[k])
                        id = grupee[l][0].dataValues.id
                    }
                    zaDodatiStudenti.push(db.student.create({ime:imena[k], prezime:prezimena[k], index:indexi[k], grupaId:id}).then(function(s){
                        return new Promise(function(resolve, reject){
                            resolve(s)
                        })
                    }))
                }
                else{
                    vecUneseni.push(students[k].dataValues.index)
                }
                
            } 
            Promise.all(zaDodatiStudenti).then(function(s){
        
                var a = s.length - vecUneseni.length
                var resp = "Dodano " + a + " studenata, a studenti " 
                
                if(vecUneseni.length != 0){
                    for(var m = 0; m<vecUneseni.length; m++){
                        if(m == vecUneseni.length-1)
                            resp += vecUneseni[m] + " već postoje!"
                        else
                        resp += vecUneseni[m]+","
                    }
                    res.send({status:resp})
                    
                }
                else{
                    res.send({status:"Dodano " + s.length +" studenata!"})
                }
                return new Promise(function(resolve, reject){
                    resolve(s)
                })
            })
        })
    })
  })
})

app.listen(3000);

module.exports = app;
