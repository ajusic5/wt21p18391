const Sequelize = require("sequelize");
const sequelize = new Sequelize("wt2118391","root","password",{
    host:"127.0.0.1",
    dialect:"mysql",
    logging:false
});
    
const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;

//import modela
db.student = require('./student.js')(sequelize);
db.grupa = require('./grupa.js')(sequelize);
db.vjezba =require('./vjezba.js')(sequelize);
db.zadatak = require('./zadatak.js')(sequelize);
//relacije
db.grupa.hasMany(db.student, {as:'studentiIzGrupe'});
db.vjezba.hasMany(db.zadatak, {as:'zadaciVjezbe'});

module.exports=db;